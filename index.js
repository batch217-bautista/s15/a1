console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:
	let firstName = "Shawn Michael";
	console.log("My full name is " + firstName);

    let lastName = "Bautista";
    console.log(lastName);

	let age = 25;
	console.log("My current age is: " + age);
	
	let hobbies = ["Reading", "Gaming", "Coding", "Watching YouTube"];
	console.log("My hobbies: ")
	console.log(hobbies);

	let workAddress = {
		houseNumber: "799 D",
		streetName: "Iznart Street",
		city: "Iloilo City Proper",
		state: "Iloilo",
	}
    console.log("Work Address: ")
    console.log(workAddress);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let hisAge = 40;
	console.log("My current age is: " + hisAge);
	
	let friends = ["Tony","Bruce","Thor", "Natasha","Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		hisName: "Steve Rogers",
		age: 40,
		isActive: false,
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let friendsName = "Bucky Barnes";
	console.log("My bestfriend is: " + friendsName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);